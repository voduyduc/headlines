import feedparser
from flask import Flask
from flask import render_template
from flask import request
import json
import urllib
import datetime
from flask import make_response

app = Flask(__name__)
RSS_FEEDS = {   'bbc': 'http://feeds.bbci.co.uk/news/rss.xml',
                'cnn': 'http://rss.cnn.com/rss/edition.rss',
                'fox': 'http://feeds.foxnews.com/foxnews/latest',
                'iol': 'http://www.iol.co.za/cmlink/1.640'}

DEFAULTS = {
    'publication':'cnn',
    'city':'London, UK',
    'currency_from':'GBP',
    'currency_to':'USD'
}

@app.route("/")
@app.route("/",methods=['GET','POST'])
@app.route("/<publication>")

def home():
    publication = get_value_with_fallback("publication")
    if not publication:
        publication = DEFAULTS['publication']
    articles = get_news(publication)
    city = get_value_with_fallback('city')
    if not city:
        city = DEFAULTS['city']
    weather = get_weather(city)
    currency_from = get_value_with_fallback('currency_from')
    if not currency_from:
        currency_from = DEFAULTS['currency_from']
    currency_to = get_value_with_fallback("currency_to")
    if not currency_to:
        currency_to = DEFAULTS['currency_to']
    rate, currencies = get_rates(currency_from,currency_to)
    response = make_response(render_template("home.html",
                                            articles=articles,
                                            weather=weather,
                                            currency_from=currency_from,
                                            currency_to=currency_to,
                                            rate=rate,
                                            currencies=sorted(currencies)))
    expires = datetime.datetime.now() + datetime.timedelta(days=365)
    response.set_cookie("publication", publication, expires=expires)
    response.set_cookie("city", city, expires=expires)
    response.set_cookie("currency_from", currency_from, expires=expires)
    response.set_cookie("currency_to", currency_to, expires=expires)
    return response

def get_weather(query):
    api_url = "http://api.openweathermap.org/data/2.5/weather?q={}&units=metric&appid=20d427d4ba0aa83131c4b473e700503f"
    query = urllib.parse.quote(query)
    url = api_url.format(query)
    data = urllib.request.urlopen(url).read()
    parsed = json.loads(data)
    weather = None
    if parsed.get("weather"):
        weather = {"description":parsed["weather"][0]["description"],
            "temperature":parsed["main"]["temp"],
            "city":parsed["name"],
            'country': parsed['sys']['country']
            }
    return weather

def get_news(publication):
    query = request.args.get("publication")
    if not query or query.lower() not in RSS_FEEDS:
        publication = DEFAULTS['publication']
    else: 
        publication = query.lower()
    feed = feedparser.parse(RSS_FEEDS[publication])
    return feed['entries']

CURRENCY_URL = "https://openexchangerates.org//api/latest.json?app_id=bd28282feb59486fb7ab8747c1d79e0b"

def get_rates(frm, to):
    all_currency = urllib.request.urlopen(CURRENCY_URL).read()
    parsed = json.loads(all_currency).get('rates')
    frm_rate = parsed.get(frm.upper())
    to_rate = parsed.get(to.upper())
    return (to_rate/frm_rate, parsed.keys())

def get_value_with_fallback(key):
    if request.args.get(key):
        return request.args.get(key)
    if request.cookies.get(key):
        return request.cookies.get(key)

if __name__ == '__main__':
    app.run(port=5000, debug = True)

